// import files
var add = require("./calculator/add");
var subtract = require("./calculator/subtract");
var multiply = require("./calculator/multiply");
var divide = require("./calculator/divide");




window.onload = function () {
  document.getElementById("toSecBtn").addEventListener("click", toSec);
  document.getElementById("toHrsBtn").addEventListener("click", toHrs);
};

function toHrs() {
  var firstNum = parseInt(document.getElementById("seconds").value);
  var resultElm = document.getElementById("result");
  var result;

  try {
   result=subtract(firstNum);
    resultElm.textContent = "Result: " + result;
    resultElm.style.color = "black";
  } catch (error) {
    resultElm.textContent = "Error: " + error.message;
    resultElm.style.color = "red";
  }
}


function toSec() {
  var firstNum = parseInt(document.getElementById("hours").value);
  var secondNum = parseInt(document.getElementById("minutes").value);
  var resultElm = document.getElementById("result");
  var result;

  try {
   result=add(firstNum,secondNum)
    resultElm.textContent = "Result: " + result;
    resultElm.style.color = "black";
  } catch (error) {
    resultElm.textContent = "Error: " + error.message;
    resultElm.style.color = "red";
  }
}
