// import files
var add = require('./calculator/add')
var subtract = require('./calculator/subtract')


// Test Case 1 (Addition)
test("Should return seconds", () => {
  expect(add(1, 0)).toBe(3600);
});

// Test Case 2 (Subtraction)
test("Should return seconds", () => {
  expect(add(1, 1)).toBe(3660);
});

test("Should return hours", () => {
  expect(subtract(3600)).toBe(1);
});

test("Should return hours", () => {
  expect(subtract(36000)).toBe(10);
});
